package com.example.tutorial1.SIgnUp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.tutorial1.LogIn.Login;
import com.example.tutorial1.Models.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.tutorial1.Utilities.Contants.SIGNUP_CUSTOMER;

public class RegisterParser extends AsyncTask<String, Void, String> {
    Context mContext;
    UserModel registerModel;


    public RegisterParser(Context mContext, UserModel model) {
        this.mContext = mContext;
        this.registerModel = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... voids) {

        URL url;
        HttpURLConnection   connection = null;
        int rescode = -1;
        InputStream in = null;
        String json = null;

        try {

            url = new URL(SIGNUP_CUSTOMER);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            //connection.setConnectTimeout(10000);
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            // connection.setRequestProperty("Authorization", TOKEN);
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());

            final JSONObject params = new JSONObject();

            try {
                params.put("password", registerModel.getPassword());
                params.put("email", registerModel.getEmail());
                params.put("username", registerModel.getUsername());
                params.put("name", registerModel.getName());
                params.put("address", registerModel.getAddress());
                params.put("contact", registerModel.getContact());

                System.out.println("gogSignIn " + registerModel.getEmail());
                System.out.println("gogSignInPass " + registerModel.getPassword());

            } catch (Exception e) {
                e.printStackTrace();
            }

            dataOutputStream.writeBytes(params.toString());

            dataOutputStream.flush();
            dataOutputStream.close();

            connection.connect();

            rescode = connection.getResponseCode();
            Log.e("resCode ", String.valueOf(rescode));


            in = connection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

            String line = "";
            final StringBuilder responseOutput = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                responseOutput.append(line);
            }

            bufferedReader.close();
            json = responseOutput.toString();
            System.out.println("Check " + responseOutput.toString());


            return json;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("SignInResponse " + s);
        JSONObject msg;
        try {
            msg = new JSONObject(s);
            String message = msg.getString("message");
            Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();

            if (message.equals("Customer Registerd Successfully")) {
                Intent intent = new Intent(mContext, Login.class);
                mContext.startActivity(intent);
            }else{
                Toast.makeText(mContext, "Signup failed", Toast.LENGTH_LONG).show();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}