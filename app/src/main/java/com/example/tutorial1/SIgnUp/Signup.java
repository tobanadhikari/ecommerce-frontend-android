package com.example.tutorial1.SIgnUp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorial1.LogIn.Login;
import com.example.tutorial1.R;
import com.example.tutorial1.Models.UserModel;

public class Signup extends AppCompatActivity {

    EditText edit_name, edit_email, edit_password, edit_address, edit_contact, edit_username;
    Button button_signup;

    TextView signup_text;

    String userfullName, userEmail, userPassword, userLoginName, userAddress, userContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_password = (EditText) findViewById(R.id.edit_password);
        edit_name = (EditText) findViewById(R.id.edit_name);
        edit_address = (EditText) findViewById(R.id.edit_address);
        edit_contact = (EditText) findViewById(R.id.edit_contact);
        edit_username = (EditText) findViewById(R.id.edit_username);


        button_signup = (Button) findViewById(R.id.button_signup);

        signup_text = (TextView) findViewById(R.id.signup_text);
        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Signup.this, Login.class);
                startActivity(intent);
            }
        });


        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userfullName = edit_name.getText().toString();
                userEmail = edit_email.getText().toString();
                userPassword = edit_password.getText().toString();
                System.out.println("Username " + userfullName);

                userLoginName = edit_username.getText().toString();
                userAddress = edit_address.getText().toString();
                userContact = edit_contact.getText().toString();

                if (TextUtils.isEmpty(userfullName)) {
                    edit_name.setError("Please enter your full name");
                    edit_name.requestFocus();
                } else if (TextUtils.isEmpty(userEmail)) {
                    edit_email.setError("Please enter your email");
                    edit_email.requestFocus();
                } else if (TextUtils.isEmpty(userPassword)) {
                    edit_password.setError("Please enter your password");
                    edit_password.requestFocus();
                } else if (TextUtils.isEmpty(userLoginName)) {
                    edit_username.setError("Please enter your username");
                    edit_username.requestFocus();
                } else if (TextUtils.isEmpty(userAddress)) {
                    edit_address.setError("Please enter your address");
                    edit_address.requestFocus();
                } else if (TextUtils.isEmpty(userContact)) {
                    edit_contact.setError("Please enter your contact number");
                    edit_contact.requestFocus();
                } else {
                    UserModel userModel = new UserModel();
                    userModel.setName(userfullName);
                    userModel.setEmail(userEmail);
                    userModel.setPassword(userPassword);
                    userModel.setUsername(userLoginName);
                    userModel.setAddress(userAddress);
                    userModel.setContact(userContact);

                    new RegisterParser(Signup.this, userModel).execute();
                }
            }
        });


    }
}
