package com.example.tutorial1.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.tutorial1.R;

public class ImageAdapter extends PagerAdapter {

    private int im_res[] = {R.drawable.uri,R.drawable.sm_ffh,R.drawable.raw,R.drawable.mulan,R.drawable.kgf,R.drawable.endgame,R.drawable.detectivepikachu};

    LayoutInflater layoutInflater;
    Context context;

    public  ImageAdapter(Context context){
        this.context=context;
    }


    @Override
    public int getCount() {
        return im_res.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view==object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_layout,null);
        ImageView imageView = view.findViewById(R.id.img_vp);
        imageView.setImageResource(im_res[position]);
        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);

    }
}
