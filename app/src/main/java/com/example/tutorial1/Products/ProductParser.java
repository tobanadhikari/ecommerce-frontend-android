package com.example.tutorial1.Products;

import android.content.Context;
import android.os.AsyncTask;

import com.example.tutorial1.Utilities.AllParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.tutorial1.Utilities.Contants.GET_ALL_PRODUCT_LIST;
import static com.example.tutorial1.Utilities.Contants.TOKEN;

public class ProductParser extends AsyncTask<String, Void, String> {

    Context context;
    AllParser allParser;

    List<Product> productList = new ArrayList<>();

    public ProductParser(Context context, AllParser allParser) {
        this.context = context;
        this.allParser = allParser;
    }

    @Override
    protected String doInBackground(String... strings) {
        URL url;
        HttpURLConnection httpURLConnection;

        try {
            url = new URL(GET_ALL_PRODUCT_LIST);
            httpURLConnection = (HttpURLConnection) url.openConnection();

            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");

            httpURLConnection.setRequestProperty("Authorization", TOKEN);

            httpURLConnection.setDoInput(true);

            int resCode = httpURLConnection.getResponseCode();

            System.out.println("ResCode = " + resCode);

            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String Line = "";
            StringBuilder responseoutput = new StringBuilder();

            while ((Line = reader.readLine()) != null) {
                responseoutput.append(Line);
            }
            System.out.println(("Response = " + responseoutput.toString()));

            return responseoutput.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("Response From Server " + s);

        try {
            JSONArray jsonArray = new JSONArray(s);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject childJsonObject = jsonArray.getJSONObject(i);

                Product product = new Product();
                product.setProduct_id(childJsonObject.getString("id"));

                product.setDescription(childJsonObject.getString("description"));
                product.setName(childJsonObject.getString("name"));
                product.setPrice(childJsonObject.getString("price"));
                product.setDom(childJsonObject.getString("dom"));
                product.setExpiry_date(childJsonObject.getString("expiryDate"));

                JSONObject catJson = childJsonObject.getJSONObject("category");
                product.setCategory_id(catJson.getString("id"));
                product.setCategory_name(catJson.getString("name"));

                JSONObject branJson = childJsonObject.getJSONObject("brand");
                product.setBrand_id(branJson.getString("id"));
                product.setBrand_name(branJson.getString("name"));

                System.out.println("Brand name = " + branJson.getString("name"));

                productList.add(product);
                allParser.processFinish(productList);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
