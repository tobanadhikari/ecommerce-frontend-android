package com.example.tutorial1.Products;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tutorial1.DetailsActivity;
import com.example.tutorial1.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyHolder> {

    Context context;
    List<Product> productArrayList;

    String prdDescription, prdName, prdId;

    public ProductAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productArrayList = productList;
    }

    @NonNull
    @Override
    public ProductAdapter.MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_products, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductAdapter.MyHolder holder, final int position) {
        Product currentItem = productArrayList.get(position);
        System.out.println("Detail= " + currentItem.getDescription());

        holder.product_name.setText(currentItem.getName());
        holder.product_description.setText(currentItem.getDescription());

        prdDescription = currentItem.getDescription();
        //queImg = currentItem.getImage();
        prdName = currentItem.getName();

        prdId = currentItem.getProduct_id();

        //new SendHttpRequestTask(holder.question_image).execute("http://192.168.0.108:3000/api/question/images/" + queImg);

        holder.main_section.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*for converting imgview to bitmap*/
              /*  holder.question_image.buildDrawingCache();
                Bitmap bitmap = holder.question_image.getDrawingCache();*/


                Intent detailIntent = new Intent(context, DetailsActivity.class);
                Product clickedItem = productArrayList.get(position);

                //detailIntent.putExtra("product_description", clickedItem.getDescription());
                //detailIntent.putExtra("question_image", bitmap);
                detailIntent.putExtra("product_name", clickedItem.getName());
                detailIntent.putExtra("product_id", clickedItem.getProduct_id());

                context.startActivity(detailIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        TextView product_description, product_name;
        //ImageView product_image;

        LinearLayout main_section;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
            product_description = (TextView) itemView.findViewById(R.id.product_description);
            product_name = (TextView) itemView.findViewById(R.id.product_name);
            //  question_image = (ImageView) itemView.findViewById(R.id.question_image);

            main_section = (LinearLayout) itemView.findViewById(R.id.main_section);
        }
    }
}
