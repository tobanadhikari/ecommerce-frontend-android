package com.example.tutorial1.LogIn;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.tutorial1.MainActivity;
import com.example.tutorial1.Models.UserModel;
import com.example.tutorial1.Utilities.ProcessToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.tutorial1.Utilities.Contants.LOGIN_CUSTOMER;
import static com.example.tutorial1.Utilities.Contants.TOKEN;

public class LoginParser extends AsyncTask<String, Void, String> {
    Context mContext;
    UserModel loginModel;


    public LoginParser(Context mContext, UserModel model) {
        this.mContext = mContext;
        this.loginModel = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... voids) {

        URL url;
        HttpURLConnection connection = null;
        int rescode = -1;
        InputStream in = null;
        String json = null;

        try {

            url = new URL(LOGIN_CUSTOMER);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            //connection.setConnectTimeout(10000);
            connection.setRequestProperty("Content-Type", "application/json");

            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            // connection.setRequestProperty("Authorization", TOKEN);
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());

            final JSONObject params = new JSONObject();

            try {
                params.put("username", loginModel.getName());
                params.put("password", loginModel.getPassword());

                System.out.println("loginName " + loginModel.getName());

            } catch (Exception e) {
                e.printStackTrace();
            }

            dataOutputStream.writeBytes(params.toString());

            dataOutputStream.flush();
            dataOutputStream.close();

            connection.connect();

            rescode = connection.getResponseCode();
            Log.e("resCode ", String.valueOf(rescode));


            in = connection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

            String line = "";
            final StringBuilder responseOutput = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                responseOutput.append(line);
            }

            bufferedReader.close();
            json = responseOutput.toString();
            System.out.println("Check " + responseOutput.toString());


            return json;

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        System.out.println("LogInResponse " + s);
        JSONObject json;
        try {
            json = new JSONObject(s);
            if (json.has("Username")) {
                String Username = json.getString("Username");
                Toast.makeText(mContext, Username, Toast.LENGTH_LONG).show();

                try {
                    ProcessToken processJSONObjet = new ProcessToken(json.toString());
                    TOKEN = processJSONObjet.getToken();
                    Log.e("Token", TOKEN);
                } catch (JSONException | NullPointerException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(mContext, MainActivity.class);
                mContext.startActivity(intent);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}