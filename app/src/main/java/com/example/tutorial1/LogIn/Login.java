package com.example.tutorial1.LogIn;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tutorial1.Models.UserModel;
import com.example.tutorial1.R;
import com.example.tutorial1.SIgnUp.RegisterParser;
import com.example.tutorial1.SIgnUp.Signup;

public class Login extends AppCompatActivity {

    EditText edit_fullname_login, edit_password_login;
    Button button_login;

    String userName, userPassword;

    TextView signup_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edit_fullname_login = (EditText) findViewById(R.id.edit_fullname_login);
        edit_password_login = (EditText) findViewById(R.id.edit_password_login);
        button_login = (Button) findViewById(R.id.button_login);

        signup_text = (TextView) findViewById(R.id.signup_text);
        signup_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this, Signup.class);
                startActivity(intent);
            }
        });
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = edit_fullname_login.getText().toString();
                userPassword = edit_password_login.getText().toString();
                System.out.println("Username " + userName);

                if (TextUtils.isEmpty(userName)) {
                    edit_fullname_login.setError("Please enter your user name");
                    edit_fullname_login.requestFocus();
                } else if (TextUtils.isEmpty(userPassword)) {
                    edit_password_login.setError("Please enter your password");
                    edit_password_login.requestFocus();
                } else {
                    UserModel userModel = new UserModel();
                    userModel.setName(userName);
                    userModel.setPassword(userPassword);
                    new LoginParser(Login.this, userModel).execute();
                }
            }
        });

    }
}
