package com.example.tutorial1.Utilities;

public class Contants {

    public static String TOKEN = "";

    public static String BASEURL = "http://192.168.0.100:8080/";

    public static final String LOGIN_CUSTOMER = BASEURL + "login";

    public static final String SIGNUP_CUSTOMER = BASEURL + "api/customers/signup";

    public static final String GET_ALL_PRODUCT_LIST = BASEURL + "api/products/";

    public static final String POST_CUSTOMERS_CART_ITEM = BASEURL + "api/customers/1/carts/cartitems/";

    public static final String GET_CUSTUMERS_CART_ITEM = BASEURL + "api/customers/1/cart/";

    public static final String GET_CUSTOTMER_PROFILE = BASEURL + "api/customers/1/";



}
