package com.example.tutorial1.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

public class ProcessToken {

    JSONObject jsonObject = null;

    public ProcessToken(String string) throws JSONException {
        jsonObject = new JSONObject(string.toString());
    }

    public String getToken() throws JSONException, NullPointerException {
        String token = jsonObject.get("Authorization").toString() == null ? null : jsonObject.get("Authorization").toString();
        return token;
    }
}
