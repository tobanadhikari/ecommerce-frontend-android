package com.example.tutorial1.Utilities;

import com.example.tutorial1.Products.Product;

import java.util.List;

public interface AllParser {
    void processFinish(List<Product> productList);

}