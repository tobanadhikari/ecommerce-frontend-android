package com.example.tutorial1.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {

    private static final String USER_PREFS = "eshop";
    private SharedPreferences appSharedPrefs;

    private SharedPreferences.Editor appSharedPrefsEditor;

    public SharedPrefs(Context context) {
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.appSharedPrefsEditor = appSharedPrefs.edit();
    }

    public String getString(String stringKeyValue){
        return appSharedPrefs.getString(stringKeyValue,"");
    }


    public void setString(String stringKeyValue, String _stringValue){
        appSharedPrefsEditor.putString(stringKeyValue,_stringValue).commit();
    }
}
